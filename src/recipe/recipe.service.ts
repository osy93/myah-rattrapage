import { Service } from '../utils/service.abstract';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { RecipeSchema } from './recipe.model';
import { IRecipe, RecipeDocument } from './recipe.document';
import { getConfiguration } from '../helpers/configuration.helper';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import { ItemService } from '../item/item.service';
import ObjectId = mongoose.Types.ObjectId;

const config: any = getConfiguration().user;

export class RecipeService extends Service<RecipeDocument> {
  constructor(model: Model<RecipeDocument>) {
    super(model, 'recipe');
  }

  public static get(): RecipeService {
    return super.getService(RecipeSchema, 'recipe', 'recipes', RecipeService);
  }

  async create(recipeData: IRecipe) {
    for (const item of recipeData.components) {
      if (!item._id) {
        const savedItem = await ItemService.get().create(item);
        item._id = savedItem._id;
      } else {
        ItemService.get().update(item._id, { fixedPrice: item.fixedPrice });
      }
    }
    if (!recipeData.item._id) {
      const savedItem = await ItemService.get().create(recipeData.item);
      recipeData.item._id = savedItem._id;
    } else {
      ItemService.get().update(recipeData.item._id, { fixedPrice: recipeData.item.fixedPrice });
    }

    const recipe = new this._model(recipeData);
    return recipe.save();
  }

  async getAll(criteria: any = {}, skip = 0, limit = config.paging.defaultValue) {
    return this._model
      .find(criteria)
      .skip(skip)
      .limit(limit);
  }

  async get(id?: ObjectId, criteria: any = {}) {
    if (id) {
      criteria._id = id;
    }
    return this._model.findOne(criteria);
  }

  async update(id: ObjectId, recipeData: any) {
    const recipe = await this._model.findById(id);
    if (!recipe) {
      throw new CustomError(CustomErrorCode.ERRNOTFOUND, 'Recipe not found');
    }

    recipe.set(recipeData);
    return recipe.save();
  }
}
