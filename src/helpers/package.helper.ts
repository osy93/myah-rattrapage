import { path } from 'app-root-path';
import { join } from 'path';

export function getPackageName(): string {
  try {
    return require(join(path, 'package.json')).name;
  } catch (err) {
    throw new Error('Cannot read package name');
  }
}
