import * as express from 'express';
import UserRoutes from './user/user.routes';
import PrivilegeRoutes from './privilege/privilege.routes';
import RoleRoutes from './role/role.routes';
import AuctionatorRoutes from './auctionator/auctionator.routes';
import ItemRoutes from './item/item.routes';
import RecipeRoutes from './recipe/recipe.routes';
import HealthRoutes from './utils/health.routes';
import { UserService } from './user/user.service';
import { RoleService } from './role/role.service';
import { PrivilegeService } from './privilege/privilege.service';
import { AuctionatorService } from './auctionator/auctionator.service';
import { ItemService } from './item/item.service';
import { RecipeService } from './recipe/recipe.service';

/**
 * Function used to configure application
 *
 * @export
 * @param {object} [configuration]
 * @returns {Promise<express.Router[]>}
 */
export function configureRouter(configuration?: object): express.Router[] {
  // Create a new router that will be exported and used by top-level app
  const router = express.Router();
  const routes = [UserRoutes, RoleRoutes, PrivilegeRoutes, HealthRoutes, AuctionatorRoutes, ItemRoutes, RecipeRoutes];

  routes.forEach(r => router.use('/api', r));

  // Create a private router
  const privateRouter = express.Router();

  // Your app-router is now configured, let's export it !
  return [router, privateRouter];
}

export function initServices() {
  [UserService, RoleService, PrivilegeService, AuctionatorService, ItemService, RecipeService].forEach(s => s.get());
}
