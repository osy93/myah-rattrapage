/**
 * created at : 20/11/18
 * by thomas
 * shop360ref
 * ioStorage
 */
import SocketIO = require('socket.io');

let ioStorage: any = null;
export function ioInstance(io?: SocketIO.Server) {
  if (io) {
    ioStorage = io;
  }

  return ioStorage;
}
