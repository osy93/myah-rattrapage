TP Finale: Forker le repo et faites le fichier CI associé.
--
Avant de débuter la CI : Envoyer moi sur teams l'adresse de votre repository forker, inviter moi dans le repository avec tout les droits que je puisse tester.
Le projet dispose de scripts disponible sur le fichier`package.json`. Ils sont exécutable via la commande`npm run $SCRIPT_NAME`.
Toutes les commandes`NPM`sont testables sur votre machine.
La CI devra au maximum utiliser les variables de l'appli Gitlab et éviter d'en mettre dans le fichier`.gitlab-ci.yml`
Je suis disponible en tant que "développeur" pour répondre à des questions basiques.


La ci devra effectuer les actions suivantes :

Push sur`master`/ ou sur un`tag`:
- Générer l'`apidoc`
- Envoyer l'api doc générée sur le serveur dans un dossier`prod` ou le nom du tag
 
Push sur`develop` : 
- Lancer un`prettier:check`
- Lancer le`tslint`
- Lancer les `test`
- Envoyer le rapport de test (`success`ou`failure`) généré sur le serveur dans un dossier test 
- Générer l'`apidoc`
- Envoyer via scp (ssh) l'api doc générée sur le serveur dans un dossier`dev`


Ressources à dispositions :
- Identifiant serveur web ssh :
    - username: student
    - password: itsasecret
    - adresse ssh: 51.75.126.124
    - port ssh : 30022
    - dossier où déposer vos contenu : /var/www/YOUR_NAME
- adresse web : `http://51.75.126.124:30080`


Avant l'execution de n'importe quel script vous devez faire un `npm install`
Commandes disponible:
- `npm run tslint` : passe le linter sur le projet
- `npm run test` : passe les tests et génère un rapport
- `npm run doc` : génère l'apidoc
- `npm run prettier:check` : ppasse le prettier sur le projet
- `npm run build` :  build le code (obligatoire avant de le lancer)
- `npm run start` :  démarre le projet

Clé privée ssh le jour J 
